import React from 'react';
import axios from 'axios';

import { withIonLifeCycle, IonPage, IonButton, IonTitle, IonHeader, IonToolbar, IonButtons, IonIcon, IonContent } from '@ionic/react';
import Card from '../../components/organismes/Card/RessourceCard';

import { chevronBack } from 'ionicons/icons';

import './trends.css';

class Trends extends React.Component {
  state = {
    ressources: [],
    news: [],
    types_ressources: [],
    url: "https://ms-lab-directus.fun/items/",
  }

  ionViewDidEnter = () => {
    this.callApiTrends.api.get(this.state.url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[DATE_PUBLICATION_RESSOURCE][_lte]=2021-08-01', {
      params: {
        limit: 20
      }
    }).then(res => {
      const all = res.data.data;
      this.setState({ ressources: all })
    })
  }

  callApiTrends = {
    api: axios.create({ baseURL: this.state.url }),

    viewNews: (e: any) => {
      this.callApiTrends.api.get(this.state.url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[DATE_PUBLICATION_RESSOURCE][_gte]=2021-10-10', {
        params: {
          limit: 20
        }
      }).then(res => {
        const all = res.data.data;
        this.setState({ ressources: all })
      })
    },
    viewMoreLike: (e: any) => {
      this.callApiTrends.api.get(this.state.url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[NOMBRE_LIKE][_gte]=7000&sort[]=-NOMBRE_LIKE', {
        params: {
          limit: 20
        }
      }).then(res => {
        const all = res.data.data;
        this.setState({ ressources: all })
      })
    },
    viewGames: (e: any) => {
      this.callApiTrends.api.get(this.state.url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Jeux', {
        params: {
          limit: 20
        }
      }).then(res => {
        const all = res.data.data;
        this.setState({ ressources: all })
      })
    },
    viewVideos: (e: any) => {
      this.callApiTrends.api.get(this.state.url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Vidéo', {
        params: {
          limit: 20
        }
      }).then(res => {
        const all = res.data.data;
        this.setState({ ressources: all })
      })
    },
    viewArticles: (e: any) => {
      this.callApiTrends.api.get(this.state.url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Article', {
        params: {
          limit: 20
        }
      }).then(res => {
        const all = res.data.data;
        this.setState({ ressources: all })
      })
    }
  }

  render() {
    return (
      <IonPage>
        <IonContent>
          <section className="categoryMarginSection">
            <IonHeader>
              <IonToolbar>
                <IonButtons slot="start">
                  <IonButton routerLink="/view/accueil">
                    <IonIcon icon={chevronBack}></IonIcon>
                  </IonButton>
                  <IonTitle>Tendances</IonTitle>
                </IonButtons>
              </IonToolbar>
              <IonToolbar className="positionEnfantToolbarCategory">
                <ul className="positionParentListNavigationCategory">
                  <li onClick={this.callApiTrends.viewNews}>Nouveautés</li>
                  <li onClick={this.callApiTrends.viewMoreLike}>Les plus likés</li>
                  <li onClick={this.callApiTrends.viewGames}>Jeux</li>
                  <li onClick={this.callApiTrends.viewVideos}>Vidéos</li>
                  <li onClick={this.callApiTrends.viewArticles}>Articles</li>
                </ul>
              </IonToolbar>
            </IonHeader>

            <div>
              {this.state.ressources.map((info, i) =>
                <Card
                  key={i}
                  navigation={info["ID_RESSOURCE"]}
                  user_image={info['IMAGE_PROFIL_MEMBRE']}
                  user_icon={info['NOM_UTILISATEUR_MEMBRE']}
                  ressource_title={info['TITRE_RESSOURCE']}
                  ressource_image={info['IMAGE_RESSOURCE']}
                  ressource_content={info["CONTENU_RESSOURCE"]}
                  ressource_nbr_like={info["NOMBRE_LIKE"]}
                  ressource_nbr_comments={info["NOMBRE_COMMENTAIRE"]}
                />
              )}
            </div>
          </section>
        </IonContent>
      </IonPage>
    );
  };
}
export default withIonLifeCycle(Trends);