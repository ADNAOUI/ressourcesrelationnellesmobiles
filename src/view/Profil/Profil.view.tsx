import React from 'react';
import { withIonLifeCycle, IonContent, IonPage } from '@ionic/react';

//molécules
import NavigationMolecules from '../../components/molecules/Profil/Navigation.molecules';
import BackgroundMolecules from '../../components/molecules/Profil/Background.molecules';
import AboutMolecules from '../../components/molecules/Profil/About.molecules';
import ApercuMolecules from '../../components/molecules/Profil/Apercu.molecules';
import DocumentsMolecules from '../../components/molecules/Profil/Documents.molecules';
//CSS
import './profil.css';

class Profil extends React.Component {

  render() {
    return (
      <IonPage>
        <IonContent>
          <section className="categoryMarginSection">
            <BackgroundMolecules />
            <NavigationMolecules />
            <AboutMolecules/>
            <ApercuMolecules/>
            <DocumentsMolecules/>
          </section>
        </IonContent>
      </IonPage>
    );
  }
};
export default withIonLifeCycle(Profil);