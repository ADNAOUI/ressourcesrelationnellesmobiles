import React, { useState } from 'react';
import axios from 'axios'

import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonInfiniteScroll, useIonViewWillEnter } from '@ionic/react';

//Import des Components
import Card from '../../components/organismes/Card/RessourceCard';
import Spinner from '../../components/molecules/Spinner/Spinner';
import ButtonSearchRelation from '../../components/molecules/Explorer/ButtonSearchRelation';

//CSS
import './explorer.css';

const Explorer = () => {

  let url = "https://ms-lab-directus.fun/items/";
  const api = axios.create({ baseURL: url });

  const [ressources, setRessources] = useState<string[]>([])
  const [isInfiniteDisabled, setInfiniteDisabled] = useState<boolean>(false);
  const [max, setMax] = useState(5);
  const [idButtonRelation, setIdButtonRelation] = useState<string>('')

  const getDatasRessourcesMoreView = () => {
    api.get(url + 'rr_ressources?access_token=scsqbcopsqcksqpo151&filter[NOMBRE_LIKE][_gte]=7000&limit=20').then(res => {
      const all = res.data.data;
      setRessources([...all])
    })
  }

  async function getDatasRessources(e: any) {
    let target = e.target.innerText;
    let relations = "buttonExplorer_" + target

    const res: Response = await fetch(url + `rr_ressources?access_token=scsqbcopsqcksqpo151&limit=${max}&filter[DESIGNATION_TYPE_RELATION][_eq]=` + target);

    if (e.target.id === relations) {
      res
        .json()
        .then(async (res) => {
          const datas = res.data

          if (res && datas && datas.length > 0) {
            setRessources([...datas]);
            setIdButtonRelation(target);
          }
          else {
            setInfiniteDisabled(true)
          }
        })
    }
  }

  async function getMoreRessources()  {
    setMax(ressources.length + 5)
    const newData: any = [];
    const min = max - 5;
    for (let i = min; i < max; i++) {
      newData.push('Card' + i);
    }
    api.get(url + `rr_ressources?access_token=scsqbcopsqcksqpo151&limit=${max}&filter[DESIGNATION_TYPE_RELATION][_eq]=` + idButtonRelation)
      .then(res => {
        setRessources([...res.data.data]);
      })
    setRessources([...ressources, ...newData]);
  }

  useIonViewWillEnter(async () => {
    getDatasRessourcesMoreView();

    return () => getDatasRessourcesMoreView()
  }, []);

  async function loadData($event: CustomEvent<void>) {
    await getMoreRessources();

    ($event.target as HTMLIonInfiniteScrollElement).complete();
  }

  return (
    <IonPage>
      <IonContent>
        <ButtonSearchRelation click={getDatasRessources} />
        <section className="marginTop10">
          <IonHeader>
            <IonToolbar>
              <IonTitle>Ressources les plus vues</IonTitle>
            </IonToolbar>
          </IonHeader>
          {ressources.map((ressource: any, i: number) =>
            <Card
              key={i}
              navigation={ressource["ID_RESSOURCE"]}
              user_image={ressource['IMAGE_PROFIL_MEMBRE']}
              user_icon={ressource['NOM_UTILISATEUR_MEMBRE']}
              ressource_title={ressource['TITRE_RESSOURCE']}
              ressource_image={ressource['IMAGE_RESSOURCE']}
              ressource_content={ressource["CONTENU_RESSOURCE"]}
              ressource_nbr_like={ressource["NOMBRE_LIKE"]}
              ressource_nbr_comments={ressource["NOMBRE_COMMENTAIRE"]}
            />
          )}

          <IonInfiniteScroll onIonInfinite={(e: CustomEvent<void>) => loadData(e)} threshold="100px" disabled={isInfiniteDisabled}>
            <Spinner />
          </IonInfiniteScroll>
        </section>
      </IonContent>
    </IonPage>
  );
};
export default Explorer;