import React, { useEffect, useState } from 'react';
import { IonAvatar, IonBackButton, IonButtons, IonCardSubtitle, IonChip, IonCol, IonContent, IonFab, IonFabButton, IonFabList, IonGrid, IonHeader, IonIcon, IonImg, IonItem, IonLabel, IonPage, IonRow, IonText, IonToolbar } from '@ionic/react';
import axios from 'axios';
import { chevronBackOutline, heartOutline, logoFacebook, logoInstagram, logoTwitter, logoVimeo, newspaperOutline, shareSocialOutline } from 'ionicons/icons';

import './showRessource.css';

const ShowRessource = () => {

	const [ressource, setRessource] = useState<any>('');

	const loadData = () => {
		const URLcourante = document.location.href.split('/');
		const id_ressource = URLcourante[4];

		axios.get(`https://ms-lab-directus.fun/items/rr_ressources/${id_ressource}?access_token=scsqbcopsqcksqpo151`).then(res => {
			const list = res.data.data
			setRessource(list);
		});
		window.scrollTo(0, 0);
	}
	useEffect(() => {
		loadData()
	}, [])

	return (
		<IonPage>
			<IonContent>
				<section className="categoryMarginSection">
					<IonHeader>
						<IonToolbar>
							<IonButtons slot="start">
								<IonBackButton icon={chevronBackOutline} defaultHref="/view/accueil" color="secondary" text={"Titre : " + ressource["TITRE_RESSOURCE"]} />
							</IonButtons>
						</IonToolbar>

						<IonGrid className="display-ressource-user-informations">
							<IonRow>
								<IonCardSubtitle className="colorBlue positionAvatarHome">
									<IonAvatar>
										<IonImg src={ressource["IMAGE_PROFIL_MEMBRE"]} alt="profil d'un membre" />
									</IonAvatar>
									<IonItem lines="none">
										<IonText className="spanNameCard">{ressource["PSEUDO_MEMBRE"]}</IonText>
									</IonItem>
								</IonCardSubtitle>
							</IonRow>
						</IonGrid>

						<div>
							<IonItem className="display-ressource-chip">
								<IonChip className="__category">
									<IonLabel color="light">{ressource["DESIGNATION_CATEGORIE_RESSOURCE"]}</IonLabel>
								</IonChip>
								<IonChip className="__relation">
									<IonLabel color="light">{ressource["DESIGNATION_TYPE_RELATION"]}</IonLabel>
								</IonChip>
								<IonChip outline className="__ressource">
									<IonLabel color="primary">{ressource["DESIGNATION_TYPE_RESSOURCE"]}</IonLabel>
								</IonChip>
							</IonItem>
						</div>

					</IonHeader>

					<IonGrid>
						<IonRow>
							<IonCol size="12" >
								<IonImg src={ressource["IMAGE_RESSOURCE"]} />
							</IonCol>
							<IonCol size="12" >
								<IonText className="display-ressource-date">{ressource["DATE_PUBLICATION_RESSOURCE"]}</IonText>
							</IonCol>
							<IonCol size="12" >
								<p className="display-ressource-content">
									{ressource["CONTENU_RESSOURCE"]}
								</p>
							</IonCol>
						</IonRow>
					</IonGrid>

					<IonGrid>
						<IonRow className="display-ressource-nbr">
							<IonCol size="4">
								<IonIcon color="primary" icon={newspaperOutline}></IonIcon>
								<IonText color="primary">{ressource["NOMBRE_COMMENTAIRE"]}</IonText>
							</IonCol>
							<IonCol size="4">
								<IonIcon color="primary" icon={heartOutline} />
								<IonText color="primary">{ressource["NOMBRE_LIKE"]}</IonText>
							</IonCol>
							<IonCol size="4">
								<IonFab horizontal="center">
									<IonFabButton color="dark">
										<IonIcon color="primary" icon={shareSocialOutline} />
									</IonFabButton>
									<IonFabList side="top">
										<IonFabButton color="secondary"><IonIcon icon={logoVimeo} /></IonFabButton>
										<IonFabButton color="secondary"><IonIcon icon={logoFacebook} /></IonFabButton>
										<IonFabButton color="secondary"><IonIcon icon={logoInstagram} /></IonFabButton>
										<IonFabButton color="secondary"><IonIcon icon={logoTwitter} /></IonFabButton>
									</IonFabList>
								</IonFab>
							</IonCol>
						</IonRow>
					</IonGrid>
				</section>
			</IonContent>
		</IonPage>
	);
}
export default ShowRessource;