import React, { useState } from 'react';
import { IonContent, IonPage, IonInfiniteScroll, useIonViewWillEnter } from '@ionic/react';
import axios from 'axios';
import Card from '../../components/organismes/Card/RessourceCard';
import Spinner from '../../components/molecules/Spinner/Spinner';

import './home.css';

const Home = () => {
  const [data, setData] = useState<string[]>([]);
  const [isInfiniteDisabled, setInfiniteDisabled] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const pushData = () => {
    let url = "https://ms-lab-directus.fun/items/";
    const api = axios.create({ baseURL: url });

    const max = data.length + 20;
    const min = max - 20;
    const newData = [];

    for (let i = min; i < max; i++) {
      newData.push('Item' + i);
    }

    api.get(url + `rr_ressources?access_token=scsqbcopsqcksqpo151&limit=${max}`)
      .then(res => {
        const datas = res.data.data
        setData([...datas.sort(() => Math.random() - 0.5)]);
        setIsLoading(false);
      })
    setData([...data, ...newData]);
  }

  const loadData = (ev: any) => {
    setTimeout(() => {
      pushData();
      ev.target.complete();
      if (data.length === 100) {
        setInfiniteDisabled(true);
      }
    }, 500);
  }

  useIonViewWillEnter(() => {
    pushData();
  });

  return (
    <IonPage>
      <IonContent>
        {isLoading ? <Spinner classSpinner="loading-spinner-relative" /> :
          <section className="marginSection">
            {data.map((item: any, i: number) => {
              return (
                <Card
                  navigation={item["ID_RESSOURCE"]}
                  key={i}
                  user_image={item['IMAGE_PROFIL_MEMBRE']}
                  user_icon={item['NOM_UTILISATEUR_MEMBRE']}
                  ressource_title={item['TITRE_RESSOURCE']}
                  ressource_image={item['IMAGE_RESSOURCE']}
                  ressource_content={item["CONTENU_RESSOURCE"]}
                  ressource_nbr_like={item["NOMBRE_LIKE"]}
                  ressource_nbr_comments={item["NOMBRE_COMMENTAIRE"]}
                />
              )
            })}
            <IonInfiniteScroll onIonInfinite={loadData} threshold="100px" disabled={isInfiniteDisabled}>
              <Spinner />
            </IonInfiniteScroll>
          </section>
        }
      </IonContent>
    </IonPage>
  );
}

export default Home;