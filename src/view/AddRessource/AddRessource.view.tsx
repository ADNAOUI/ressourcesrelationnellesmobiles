import { useState } from 'react';
import { IonText, IonItem, IonInput, IonLabel, IonTitle, IonButton, IonContent, IonPage, IonHeader } from '@ionic/react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import CategoryMolecules from '../../components/molecules/AddRessource/Category.molecules';
import RessourceMolecules from '../../components/molecules/AddRessource/Ressource.molecules';
import RelationMolecules from '../../components/molecules/AddRessource/Relation.molecules';
import ContentMolecules from '../../components/molecules/AddRessource/Content.molecules';
import ImageMolecules from '../../components/molecules/AddRessource/Image.molecules';

import './addRessource.css';

const AddRessource = (props: any) => {
    const url = "https://ms-lab-directus.fun/items/";
    const [title, setTitle] = useState<string>('');
    const [elementCategory, setElementCategory] = useState<string>('');
    const [elementRessource, setElementRessource] = useState<string>('');
    const [elementRelation, setElementRelation] = useState<string>('');
    const [elementContent, setElementContent] = useState<string>('');
    const [imageUrl, setImageUrl] = useState<string>('');

    const datasToSubmit = {
        DATE_PUBLICATION_RESSOURCE: new Date().toJSON().slice(0, 10),
        TITRE_RESSOURCE: title,
        DESIGNATION_CATEGORIE_RESSOURCE: elementCategory,
        DESIGNATION_TYPE_RESSOURCE: elementRessource,
        DESIGNATION_TYPE_RELATION: elementRelation,
        CONTENU_RESSOURCE: elementContent,
        IMAGE_RESSOURCE : imageUrl,
    }

    const handleSubmitDatas = () => {

        if (title === "") {
            alert("Veuillez nommer votre ressource");
            return false;
        }
        else {
            axios.post(url + 'rr_ressources?access_token=scsqbcopsqcksqpo151', datasToSubmit).then(res => {
                if (res.status === 200)
                    window.location.pathname = '/'
            })
        }
    }

    return (
        <IonPage color="secondary">
            <IonContent>
                <IonTitle color="secondary" className="marginSection textCenterTitleAddRessources"><IonText>Ajouter une Ressource</IonText></IonTitle>
                <section className="sectionParentAddRessources">
                    <IonItem slot="end" lines="full" className="textCenterTitleAddRessources marginTop10">
                        <IonLabel color="primary">Titre de la ressource : </IonLabel>
                        <IonInput autocapitalize="on" id="cardTitleRessource" value={title} placeholder="votre titre ..." onIonChange={e => setTitle(e.detail.value!)} required></IonInput>
                    </IonItem>

                    <CategoryMolecules value={elementCategory} setFunction={(e: any) => setElementCategory(e.target.innerText!)} />
                    <RessourceMolecules value={elementRessource} setFunction={(e: any) => setElementRessource(e.target.innerText!)} />
                    <RelationMolecules value={elementRelation} setFunction={(e: any) => setElementRelation(e.target.innerText!)} />
                    <ContentMolecules value={elementContent} setFunction={(e: any) => setElementContent(e.detail.value!)} />
                    <ImageMolecules value={imageUrl} setFunction={(e: any) => setImageUrl(e.detail.value!)}/>

                    <div className="buttonValiderAndAnnulerAddRessources">
                        <Link to="/view/accueil">
                            <IonButton type="submit" className="ion-margin-top" color="dark">Annuler</IonButton>
                        </Link>
                        <IonButton type="submit" className="ion-margin-top" color="secondary" onClick={handleSubmitDatas}>Valider</IonButton>
                    </div>
                </section>
            </IonContent>
        </IonPage>
    );
}

export default AddRessource;