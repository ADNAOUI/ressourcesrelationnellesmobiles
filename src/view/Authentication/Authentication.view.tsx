import React, { RefObject, useState } from 'react';
import { IonText, IonIcon, IonButtons, IonButton, IonContent, IonPage, IonImg } from '@ionic/react';

/*Composant*/
import Inscription from '../../components/molecules/Authentication/Inscription.mocules';
import Connexion from '../../components/molecules/Authentication/Connexion.molecules';

/*CSS*/
import './authentication.css'
import Logo from '../../components/atoms/img/Logo-v2.png'

const Authentication = () => {
    const [indexTabsConnect, setIndexTabsConnect] = useState<boolean>()
    const headerRef: RefObject<HTMLIonHeaderElement> = React.createRef();

    const closeModal = async () => {
        await (headerRef.current!.closest('ion-modal') as HTMLIonModalElement).dismiss();
    }

    return (
        <IonPage color="secondary">
            <section ref={headerRef} >
                <IonButtons className="positionCloseCrossModalInscription" slot="end">
                    <IonButton onClick={() => closeModal()}>
                        <IonIcon color="light" name="close" slot="icon-only"></IonIcon>
                    </IonButton>
                </IonButtons>
            </section>

            <IonContent className="ion-padding relativeParentPosition">
                <IonImg src={Logo} className="authentication-image-header" />

                <section className="positionParentListNavigationModal">
                    <IonText className={indexTabsConnect ? "is-actived" : ""} onClick={() => setIndexTabsConnect(true)}> S'inscrire</IonText>
                    <IonText className={!indexTabsConnect ? "is-actived" : ""} onClick={() => setIndexTabsConnect(false)}> Se Connecter</IonText>
                </section>

                {indexTabsConnect ? <Inscription /> : <Connexion />}

            </IonContent>
        </IonPage>
    );
}

export default Authentication