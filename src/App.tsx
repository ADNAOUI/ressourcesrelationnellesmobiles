import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { IonReactRouter } from '@ionic/react-router';
import {
  IonApp,

} from '@ionic/react';


/* CSS */
import '@ionic/react/css/core.css';
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
import './variables.css';

import ShowRessource from './view/ShowRessource/ShowRessource';

import Tabs from './components/molecules/App/Tabs'
import Header from './components/molecules/App/Header'

export const App: React.FC = () => {

  return (
    <IonApp>
      <Header />

      <IonReactRouter>
        <Switch>
          <Route path="/view">
            <Tabs />
          </Route>
          <Route path="/ressource">
            <ShowRessource />
          </Route>
          <Redirect exact path="/" to="view/accueil" />
        </Switch>
      </IonReactRouter>
    </IonApp>
  );
}

export default App;