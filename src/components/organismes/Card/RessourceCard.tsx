import {
  IonItem,
  IonTitle,
  IonLabel,
  IonIcon,
  IonText,
  IonAvatar,
  IonCard,
  IonCardSubtitle,
  IonImg,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
} from '@ionic/react';
import { heart, chatbubblesOutline } from 'ionicons/icons';
import { Link } from 'react-router-dom';

import './ressourceCard.css'

const RessourceCard = (props: any) => {
  return (
    <>
      <Link to={`/ressource/${props.navigation}`}>
        <IonCard className="formatCard">
          <IonCardHeader>
            <IonCardSubtitle className="colorBlue positionAvatarHome">
              <IonAvatar>
                <IonImg src={props.user_image} alt="profil d'un membre" />
              </IonAvatar>
              <IonItem lines="none">
                <span className="spanNameCard">{props.user_icon}</span>
              </IonItem>
            </IonCardSubtitle>
            <IonItem lines="none">
              <IonCardTitle className="colorPink positionTextTitleCard">{props.ressource_title}</IonCardTitle>
            </IonItem>
          </IonCardHeader>

          <IonCardContent id="ressourceCardId">
            <IonItem lines="none">
              <IonImg className="position-image-ressource"
                src={props.ressource_image} alt="représentation de la ressource" />
            </IonItem>
            <IonText color="white">{props.ressource_content}</IonText>
          </IonCardContent>

          <IonText className="footerCard">
            <IonTitle>
              <IonIcon color="pinkofficial" className="tailleIconFooterCard" icon={heart} />
              <IonLabel>{props.ressource_nbr_like}</IonLabel>
            </IonTitle>

            <IonTitle>
              <IonIcon color="primary" className="tailleIconFooterCard" icon={chatbubblesOutline} />
              <IonLabel>{props.ressource_nbr_comments}</IonLabel>
            </IonTitle>
          </IonText>
        </IonCard>
      </Link>
    </>
  );
};
export default RessourceCard;