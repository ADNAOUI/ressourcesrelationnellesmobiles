import { Redirect, Route } from 'react-router-dom';
import {
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from '@ionic/react';
import { addCircleOutline } from 'ionicons/icons';

//Vue
import Home from '../../../view/Home/Home.view';
import Tendances from '../../../view/Trends/Trends.view';
import Profil from '../../../view/Profil/Profil.view';
import Ajouter from '../../../view/AddRessource/AddRessource.view';
import Explorer from '../../../view/Explorer/Explorer.view';


export const Tabs = () => {

  return (
    <IonTabs>
      <IonRouterOutlet>
        <Route path="/" render={() => <Redirect to="view/accueil" />} exact={true} />
        <Route path="/view/accueil" component={Home} />
        <Route path="/view/explore" component={Explorer} />
        <Route path="/view/ajouter" component={Ajouter} />
        <Route path="/view/tendances" component={Tendances} />
        <Route path="/view/profil" component={Profil} />
      </IonRouterOutlet>

      <IonTabBar className="positionTabs" slot="bottom">
        <IonTabButton className="widthTabParent" tab="accueil" href="/view/accueil">
          <IonIcon className="fas fa-home"/>
          <IonLabel>Accueil</IonLabel>
        </IonTabButton>

        <IonTabButton className="widthTabParent" tab="explore" href="/view/explore">
          <IonIcon className="fas fa-compass" />
          <IonLabel>Découvrir</IonLabel>
        </IonTabButton>

        <IonTabButton className="widthTabParent" tab="ajouter" href="/view/ajouter">
          <IonIcon className="sizeButtonAddRessources" icon={addCircleOutline} />
        </IonTabButton>

        <IonTabButton className="widthTabParent" tab="trends" href="/view/tendances">
          <IonIcon className="fas fa-fire"/>
          <IonLabel>Tendances</IonLabel>
        </IonTabButton>

        <IonTabButton className="widthTabParent" tab="profil" href="/view/profil">
          <IonIcon className="fas fa-user-cog"/>
          <IonLabel>Profil</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  );
}

export default Tabs;