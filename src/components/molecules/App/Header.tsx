import { useState } from 'react';

import {
  IonIcon,
  IonHeader, IonToolbar, IonImg, IonModal
} from '@ionic/react';

/*IMAGES*/
import Logo from '../../atoms/img/Logo_REsources_RElationnellesResponsive.png'
import ModalAccount from '../../../view/Authentication/Authentication.view'
import SearchBar from './SearchBar';

export const Header = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <IonHeader>
      <IonToolbar className="tailleToolBarApp">
        <div className="header-elements">
          <a href="/view/accueil">
            <IonImg className="imageTailleApp" src={Logo} />
          </a>
          <SearchBar />
          <IonIcon onClick={() => setShowModal(true)} color="pinkofficial" className="positionIconAccountTopBar fas fa-user-alt">
            <IonModal isOpen={showModal} onDidDismiss={() => setShowModal(false)}>
              <ModalAccount />
            </IonModal>
          </IonIcon>
        </div>
      </IonToolbar>
    </IonHeader>
  );
}

export default Header;