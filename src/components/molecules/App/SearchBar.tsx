import * as React from 'react';
import { IonSearchbar } from '@ionic/react';

class SearchBar extends React.Component {

    handleInput(event: any) {
        const query = event.target.value.toLowerCase();
        const items = document.getElementsByTagName('ion-card')
        requestAnimationFrame(() => {
            for (const item of items) {
                const shouldShow = item.textContent!.toLowerCase().indexOf(query) > -1;
                shouldShow ? item.setAttribute("style", "display: block") : item.setAttribute("style", "display: none");
            }
        });
    }

    componentDidMount = () => {
        const searchbar = document.getElementById('searchDataInCards')!
        searchbar.addEventListener('ionInput', this.handleInput)
    }

    render() {
        return (
            <IonSearchbar id="searchDataInCards" className="placementSearchBar" />
        );
    }
};

export default SearchBar