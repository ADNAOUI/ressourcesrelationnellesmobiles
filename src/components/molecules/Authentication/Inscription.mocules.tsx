import React, { useState, RefObject } from "react";
import { useForm } from "react-hook-form";

import { IonLabel, IonInput, IonItem, IonButton, IonText, IonIcon } from '@ionic/react';
import axios from 'axios';
import { alertCircleOutline } from "ionicons/icons";

type OptionType = {
    label: string;
    props: any;
    required: boolean;
    requiredOptions: any;
};

const bcrypt = require('bcryptjs');

const Inscription = () => {
    let salt = bcrypt.genSaltSync(10);

    const [pseudo, setPseudo] = useState<string>('');
    const [name, setName] = useState<string>('');
    const [firstName, setFirstName] = useState<string>('');
    const [mail, setMail] = useState<string>('');
    const [birthDate, setBirthDate] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [confirmPassword, setConfirmPassword] = useState<string>('');

    const headerRef: RefObject<HTMLFormElement> = React.createRef();

    const datasToSubmit: any = {
        NOM_UTILISATEUR_MEMBRE: pseudo,
        NOM_MEMBRE: name,
        PRENOM_MEMBRE: firstName,
        DATE_NAISSANCE: birthDate,
        MAIL_MEMBRE: mail,
        PASSWORD_MEMBRE: bcrypt.hashSync(password, salt),
    }

    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: "onTouched",
        reValidateMode: "onChange"
    });

    const onSubmit = () => {
        let url = "https://ms-lab-directus.fun/items/rr_membres?access_token=scsqbcopsqcksqpo151";

        if (password !== confirmPassword) {
            alert("Vos mots de passes sont différents")
        }
        else {
            axios.post(url, datasToSubmit).then(res => {
                if (res.status === 200)
                    window.location.pathname = '/'
            })
        }
    }

    const fields: OptionType[] = [
        {
            label: "Pseudonyme",
            required: true,
            requiredOptions: {
                maxLength: 10
            },
            props: {
                name: "Pseudonyme",
                type: "text",
                clearOnEdit: true,
                inputmode: "text",
                onIonChange: (e: any) => setPseudo(e.detail.value!),
            },
        },
        {
            label: "Nom",
            required: true,
            requiredOptions: {
            },
            props: {
                name: "Nom",
                type: "text",
                clearOnEdit: true,
                inputmode: "text",
                onIonChange: (e: any) => setName(e.detail.value!),
            }
        },
        {
            label: "Prénom",
            required: true,
            requiredOptions: {
            },
            props: {
                name: "Prénom",
                type: "text",
                clearOnEdit: true,
                inputmode: "text",
                onIonChange: (e: any) => setFirstName(e.detail.value!),
            }
        },
        {
            label: "Date de naissance",
            required: true,
            requiredOptions: {
            },
            props: {
                name: "birthDate",
                type: "date",
                clearOnEdit: true,
                inputmode: "none",
                onIonChange: (e: any) => setBirthDate(e.detail.value!),
            }
        },
        {
            label: "Adresse mail",
            required: true,
            requiredOptions: {
            },
            props: {
                name: "email",
                type: "email",
                clearOnEdit: true,
                inputmode: "email",
                onIonChange: (e: any) => setMail(e.detail.value!),
            }
        },
        {
            label: "Mot de passe",
            required: true,
            requiredOptions: {
                minlength: 8,
            },
            props: {
                name: "Password",
                type: "password",
                onIonChange: (e: any) => setPassword(e.detail.value!),
            }
        },
        {
            label: "Confirmer votre mot de passe",
            required: true,
            requiredOptions: {
                minlength: 8,
            },
            props: {
                name: "Password",
                type: "password",
                onIonChange: (e: any) => setConfirmPassword(e.detail.value!),
            }
        },
    ];

    return (
        <form className="inscriptionModalForm" ref={headerRef} onSubmit={handleSubmit(onSubmit)}>
            {fields.map((field, i) => {
                const { label, required, requiredOptions, props } = field;
                return (
                    <IonItem className={errors[props.name] ? "red-error" : ""} key={`form_field_${i}`} lines="full">
                        {required && errors[props.name] && <IonIcon className="error-inputs-inscription" icon={alertCircleOutline} color="danger" />}
                        <>
                            <IonLabel color="light" position="floating">{label}</IonLabel>
                            <IonInput {...props} {...register(props.name, { required, ...requiredOptions })} />
                        </>
                    </IonItem>
                );
            })}

            <IonText className="textPolitiqueConfidantial">
                <p>
                    En cliquant sur S’incrire, vous reconnaissez avoir lu et approuvé les
                    <IonText color="primary"> Conditions d’utilisations</IonText>  et
                    <IonText color="primary"> la Politique de confidentialité</IonText>.
                </p>
            </IonText>

            <IonButton type="submit" color="secondary" className="ion-margin-top" expand="block">S'inscrire</IonButton>
        </form>
    );
}
export default Inscription;