import { IonLabel, IonInput, IonItem, IonButton, IonText } from '@ionic/react';


const Connexion = (props: any) => {
    return (
        <>
            <form className="connexionModalForm" >
                <IonItem>
                    <IonLabel color="light" position="floating">Identifiant</IonLabel>
                    <IonInput />
                </IonItem>

                <IonItem >
                    <IonLabel color="light" position="floating">Mot de passe</IonLabel>
                    <IonInput type="password" />
                </IonItem>

                <IonText className="textPasswordForgotten">
                    <p>
                        Mot de passe oublié ?
                    </p>
                </IonText>

                <IonButton color="secondary" type="submit" className="ion-margin-top" expand="block">Se Connecter</IonButton>
            </form>
        </>
    );
}
export default Connexion;