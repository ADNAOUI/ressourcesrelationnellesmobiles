import React from 'react';
import { withIonLifeCycle, IonItem, IonLabel, IonText, IonAvatar, IonCardSubtitle, IonImg } from '@ionic/react';

//CSS
import './profil.molecule.css';

class BackgroundMolecules extends React.Component {

  render() {
    return (
      <section className="sectionEnfantProfil backgroundTopProfil">
        <IonCardSubtitle className="colorBlue positionParentAvatarProfil">
          <IonAvatar className="positionEnfantAvatarProfil">
            <IonImg src="https://i.pinimg.com/originals/43/60/72/436072ec632935ceef8af5a0cfbb2924.jpg" />
          </IonAvatar>
          <IonItem lines="none">
            <IonText className="spanNameCard">Farid Noxus</IonText>
          </IonItem>
        </IonCardSubtitle>
        <div className="parentItemsTopProfil">
          <IonText>
            <IonLabel>1000&nbsp;</IonLabel>
            <IonLabel>Vues</IonLabel>
          </IonText>
          <IonText>
            <IonLabel>1000&nbsp;</IonLabel>
            <IonLabel>Favoris</IonLabel>
          </IonText>
          <IonText>
            <IonLabel>1000&nbsp;</IonLabel>
            <IonLabel>Commentaires</IonLabel>
          </IonText>
          <IonText>
            <IonLabel>1000&nbsp;</IonLabel>
            <IonLabel>Likes</IonLabel>
          </IonText>
        </div>
      </section>
    );
  }
};

export default withIonLifeCycle(BackgroundMolecules);
