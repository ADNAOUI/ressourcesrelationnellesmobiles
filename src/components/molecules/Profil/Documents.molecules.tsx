import React from 'react';
import { IonText, IonTitle, withIonLifeCycle, IonImg } from '@ionic/react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

//CSS
import './profil.molecule.css';

class DocumentsMolecules extends React.Component {

  render() {
    return (
      <section className="categoryMarginSection">
        <IonText>
          <IonTitle>Images récentes</IonTitle>
        </IonText>
        <Carousel>
          <div>
            <IonImg src="https://www.kolpaper.com/wp-content/uploads/2021/01/Escanor-Wallpaper-12.jpg" />
          </div>
          <div>
            <IonImg src="https://www.wallpapertip.com/wmimgs/10-108260_darius-skin-god-king.jpg" />
          </div>
          <div>
            <IonImg src="https://lolskinshop.com/wp-content/uploads/2019/11/splash-art-nightbringer-vladimir.jpeg" />
          </div>
        </Carousel>
      </section>
    );
  }
};
export default withIonLifeCycle(DocumentsMolecules);