import React from 'react';
import { withIonLifeCycle, IonContent, IonPage } from '@ionic/react';

//CSS
import './profil.molecule.css';

class Navigation extends React.Component {

  render(){
    return (
      <nav id="navigationInProfil">
        <ul className="navigation-profil navBarProfil">
          <li>home</li>
          <li>gallerie</li>
          <li>favoris</li>
          <li>ressources</li>
          <li>à propos</li>
        </ul>
      </nav>
    );
  }
};

export default withIonLifeCycle(Navigation);
