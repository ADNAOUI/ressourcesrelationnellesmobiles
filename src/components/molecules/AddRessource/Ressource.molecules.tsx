import { useState, useEffect } from "react";
import { IonChip, IonHeader, IonToolbar, IonButton, IonText } from '@ionic/react';
import axios from 'axios';

const Ressource = (props:any) => {
    const url = "https://ms-lab-directus.fun/items/"
    const [ressources, setRessource] = useState([])
    const [data, setData] = useState<string>();

    const handleClick = (e: any) => {
        let targetId = e.target.id;
        const getValueSpan = document.getElementById(targetId)?.innerText!;

        // const element: HTMLElement = document.getElementById('ressourceWanted') as HTMLElement
        // const datasss = element.innerHTML

        setData(getValueSpan)
    }

    useEffect(() => {
        let isActive = true;

        axios.get(url + 'rr_type_ressources?access_token=scsqbcopsqcksqpo151&sort=POSITION_TYPE_RESSOURCE').then(res => {
            if (isActive)
                setRessource(res.data.data);
        });
        return () => { isActive = false };
    }, []);

        return (
            <>
                <section className="addRessourcesMarginSection secondFormParentAddRessources">
                    <IonHeader>
                        <IonToolbar>
                            <IonText>Type de Ressources :&nbsp;</IonText>&nbsp;
                            <IonText id="ressourceWanted" color="primary">{data}</IonText>
                        </IonToolbar>
                    </IonHeader>
                    <div className="enfantFormAddRessourcePositionElements">
                        {ressources.map((ressource, i) =>
                            <IonChip
                                key={i}
                                outline
                                color="secondary"
                            >
                                <IonButton
                                    id={"ressource " +ressource["DESIGNATION_RESSOURCE"]}
                                    color="primary"
                                    size='small'
                                    fill='clear'
                                    onClick={handleClick}
                                    onIonFocus={props.setFunction}
                                >
                                    {ressource["DESIGNATION_RESSOURCE"]}
                                </IonButton>
                            </IonChip>
                        )}
                    </div>
                </section>
            </>
        );
}
export default Ressource;
