import { IonItem, IonHeader, IonToolbar, IonTitle, IonList, IonInput, IonIcon } from '@ionic/react';
import { linkOutline } from 'ionicons/icons';

function Image(props: any) {
    return (
        <>
            <section className="addRessourcesMarginSection fourthFormParentAddRessources">
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>URL de votre Image</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonList>
                    <IonItem>
                        <IonInput value={props.value} onIonChange={props.setFunction} required></IonInput>
                        <IonIcon icon={linkOutline} slot="start" />
                    </IonItem>
                </IonList>
            </section>
        </>
    );
}
export default Image;
