import { useState, useEffect } from "react";
import { IonChip, IonHeader, IonToolbar, IonButton, IonText } from '@ionic/react';
import axios from 'axios';


const Relation = (props:any) => {
    const url = "https://ms-lab-directus.fun/items/"
    const [relations, setRelation] = useState([])
    const [data, setData] = useState<string>();


    const handleClick = (e: any) => {
        let targetId = e.target.id;
        const getValueSpan = document.getElementById(targetId)?.innerText!;

        setData(getValueSpan)
    }

    useEffect(() => {
        let isActive = true;

        axios.get(url + 'rr_type_relations?access_token=scsqbcopsqcksqpo151').then(res => {
            if (isActive)
            setRelation(res.data.data);
        });
        return () => { isActive = false };
    }, []);

    return (
            <>
                <form className="addRessourcesMarginSection thirdFormParentAddRessources">
                    <IonHeader>
                        <IonToolbar>
                            <IonText>Type de Relations :&nbsp;</IonText>&nbsp;
                            <IonText id="relationWanted" color="secondary">{data}</IonText>
                        </IonToolbar>
                    </IonHeader>
                    <div className="enfantFormAddRessourcePositionElements">
                        {relations.map((relation, i) =>
                            <IonChip key={i} color="secondary" >
                                <IonButton
                                    id={"relation" +relation["DESIGNATION_TYPE_RELATION"]}
                                    color="light"
                                    size='small'
                                    fill='clear'
                                    onClick={handleClick}
                                    onIonFocus={props.setFunction}
                                >
                                    {relation["DESIGNATION_TYPE_RELATION"]}
                                </IonButton>
                            </IonChip>
                        )}
                    </div>
                </form>
            </>
        );
}
export default Relation;
