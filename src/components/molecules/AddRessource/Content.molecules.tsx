import { IonItem, IonTextarea, IonHeader, IonToolbar, IonTitle, IonList } from '@ionic/react';

function Content (props:any) {
    return (
        <>
            <section className="addRessourcesMarginSection fourthFormParentAddRessources">
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Contenu</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonList>
                    <IonItem>
                        <IonTextarea value={props.value} onIonChange={props.setFunction} placeholder="écrire ..."></IonTextarea>
                    </IonItem>
                </IonList>
            </section>
        </>
    );
}
export default Content;
