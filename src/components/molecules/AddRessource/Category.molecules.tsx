import { useState, useEffect } from "react";
import { IonChip, IonHeader, IonToolbar, IonButton, IonText } from '@ionic/react';
import axios from 'axios';


const Category = (props: any) => {
    const url = "https://ms-lab-directus.fun/items/"
    const [categorys, setCategorys] = useState([])
    const [data, setData] = useState<string>();

    const handleClick = (e:any) => {
        let targetId = e.target.id;
        const getValueSpan = document.getElementById(targetId)?.innerText!;

        setData(getValueSpan)
    }

    useEffect(() => {
        let isActive = true;

        axios.get(url + 'rr_categories?access_token=scsqbcopsqcksqpo151&sort=POSITION_TYPE_RESSOURCE').then(res => {
            if (isActive)
                setCategorys(res.data.data);
        });
        return () => { isActive = false };
    }, []);

    return (
        <>
            <section className="firstFormParentAddRessources">
                <IonHeader>
                    <IonToolbar>
                        <IonText>Catégories souhaitée :&nbsp;</IonText>&nbsp;
                        <IonText id="categoryWanted" color='primary' >{data}</IonText>
                    </IonToolbar>
                </IonHeader>
                <div className="enfantFormAddRessourcePositionElements">
                    {categorys.map((category, i) =>
                        <IonChip
                            key={i}
                            color="noxus"
                        >
                            <IonButton
                                id={"category " + category["DESIGNATION_CATEGORIE_RESSOURCE"]}
                                size='small'
                                fill='clear'
                                onClick={handleClick}
                                onIonFocus={props.setFunction}
                            >
                                {category["DESIGNATION_CATEGORIE_RESSOURCE"]}
                            </IonButton>
                        </IonChip>
                    )}
                </div>
            </section>
        </>
    );
}
export default Category;
