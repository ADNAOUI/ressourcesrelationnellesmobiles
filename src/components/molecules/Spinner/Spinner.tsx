//*----- COMPOSANTS -----//
import Logo from '../../atoms/img/Logo_REsources_RElationnellesResponsive.png'

//*----- Styles -----//
import './spinner.css'

const Spinner = (props:any) => {
    return (
        <>
        <div className={props.classSpinner}>
            <div id="parentLoadingData" className="parent-loader">
                <div className="loader"></div>
                <div className="loader-picture">
                    <img src={Logo} alt="logo ressources relationnelles" />
                </div>
            </div>
        </div>
        </>
    )
}

export default Spinner;