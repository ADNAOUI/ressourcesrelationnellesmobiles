import * as React from "react";

import { IonLabel, IonButton, IonIcon } from '@ionic/react';
import axios from 'axios';

const ButtonSearchRelation = (props: any) => {
    const [relations, setRelations] = React.useState<string[]>([])
    let url = "https://ms-lab-directus.fun/items/";
    const api = axios.create({ baseURL: url });

    const getDatasRelations = () => {
        api.get(url + 'rr_type_relations?access_token=scsqbcopsqcksqpo151').then(res => {
            const all = res.data.data;
            setRelations([...all])
        })
    }

    React.useEffect(() => {
        getDatasRelations()
    }, []);

    return (
        <section className="marginSection">
            <div className="bouton-explorer">
                {relations.map((relation: any, i: number) =>
                    <IonButton color="dark" key={i} className="button-relation" onClick={props.click}>
                        <div className="items-relations" style={{background : relation["background"] }} >
                            <IonIcon color={relation["color"]} className={relation["icon"]}></IonIcon>
                            <IonLabel id={"buttonExplorer_" + relation["DESIGNATION_TYPE_RELATION"]} className="button-relation-text">
                                {relation["DESIGNATION_TYPE_RELATION"]}
                            </IonLabel>
                        </div>
                    </IonButton>
                )}
            </div>
        </section>
    );
}
export default ButtonSearchRelation;